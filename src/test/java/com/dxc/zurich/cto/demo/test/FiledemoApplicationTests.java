package com.dxc.zurich.cto.demo.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.dxc.zurich.cto.demo.controller.FileController;
import com.dxc.zurich.cto.demo.service.FileStorageService;

import org.junit.Assert;

@RunWith(SpringRunner.class)
@WebMvcTest(value = FileController.class)
public class FiledemoApplicationTests {
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private FileStorageService service;
	
	@Test
	public void testSum() throws Exception {
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/sum").param("x", "5").param("y", "6");
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		Assert.assertEquals("11", result.getResponse().getContentAsString());
	}
	

}
