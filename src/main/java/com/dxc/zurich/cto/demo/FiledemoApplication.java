package com.dxc.zurich.cto.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import com.dxc.zurich.cto.demo.property.FileStorageProperties;

@SpringBootApplication
@EnableConfigurationProperties({ FileStorageProperties.class })
public class FiledemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(FiledemoApplication.class, args);
	}
}
